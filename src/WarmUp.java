import java.util.Scanner;

public class WarmUp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите время, которое имеется в распоряжении фирмы:");
        int time = scanner.nextInt();
        System.out.println("Введите количество заказов:");
        int orderNums=scanner.nextInt();
        int[] workPay = new int[orderNums];
        System.out.println("Введите значения стоимости заказов:");
        for (int i = 0; i < orderNums; i++){
            System.out.println("Введите " + (i + 1) + " значение");
            workPay[i] = scanner.nextInt();
        }
        int readyPay = 0;
        int i;
        int j = 1;
        int lessElem;
        while (true) {
            i = 1;
            while (true) {
                if (workPay[i] > workPay[i - 1]) {
                    lessElem = workPay[i - 1];
                    workPay[i - 1] = workPay[i];
                    workPay[i] = lessElem;
                    i++;
                } else {
                    i++;
                }
                if (i >= workPay.length) {
                    break;
                }
            }
            j++;
            if (j >= workPay.length) {
                break;
            }
        }
        for (i = 0;i < time && i < workPay.length;i++){
            readyPay += workPay[i];
        }
        System.out.println("За имеющееся время фирма может получить: " + readyPay);
    }
}
